package main

type (
	StateID int

	DFA struct {
		states map[StateID]*DFAStateMeta
	}

	DFAStateMeta struct {
		trans     map[byte]StateID
		accepting bool
	}
)
