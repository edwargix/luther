package main

import (
	"fmt"
	"log"
	"os"
)

func usage() {
	log.Fatalf("Usage: %s definition_file source_file output_file",
		os.Args[0])
}

func main() {
	if len(os.Args) < 4 {
		usage()
	}

	scanDefnFile, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer scanDefnFile.Close()

	sourceFile, err := os.Open(os.Args[2])
	if err != nil {
		log.Fatal(err)
	}
	defer sourceFile.Close()

	outFile, err := os.OpenFile(os.Args[3],
		os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer outFile.Close()

	tokenDFAs, alphabet := readScanDefnFile(scanDefnFile)

	tokens := tokenizeSourceFile(sourceFile, tokenDFAs, alphabet)

	for _, tok := range tokens {
		fmt.Fprintf(outFile, "%s %s %d %d\n", tok.id, tok.val, tok.line, tok.column)
	}
}
