package main

import "reflect"

type Set map[interface{}]struct{}

func (set Set) Equals(other Set) bool {
	return reflect.DeepEqual(set, other)
}

func (set Set) DoesIntersect(other Set) bool {
	for elem := range set {
		if _, ok := other[elem]; ok {
			return true
		}
	}
	return false
}

func (set Set) Add(elem interface{}) {
	set[elem] = struct{}{}
}

func (set Set) Del(elem interface{}) {
	delete(set, elem)
}

func (set Set) Contains(elem interface{}) bool {
	for other, _ := range set {
		if elem == other {
			return true
		}
	}
	return false
}

func (set Set) Pop() interface{} {
	var first interface{}
	for elem, _ := range set {
		first = elem
		break
	}
	delete(set, first)
	return first
}

func (set Set) Size() int {
	return len(set)
}

func (set Set) Copy() Set {
	s := make(Set)
	for k, v := range set {
		s[k] = v
	}
	return s
}
