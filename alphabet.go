package main

import "fmt"

func alphabetEncode(buf []byte) string {
	s := ""
	for _, b := range buf {
		if (b >= '0' && b <= '9') ||
			(b >= 'A' && b <= 'Z') ||
			(b >= 'a' && b <= 'w') ||
			(b >= 'y' && b <= 'z') {
			s += string(b)
		} else {
			s += "x"
			s += fmt.Sprintf("%02x", b)
		}
	}
	return s
}
