package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type TokenDFA struct {
	tokenID string
	val     string
	dfa     *DFA
}

func readDFATableFile(f *os.File, alphabet []byte) *DFA {
	dfa := DFA{}
	dfa.states = make(map[StateID]*DFAStateMeta)

	lineScanner := bufio.NewScanner(f)
	for lineScanner.Scan() {
		parts := strings.FieldsFunc(lineScanner.Text(), func(c rune) bool {
			return c == ' ' || c == '\t'
		})
		state := DFAStateMeta{}
		switch parts[0] {
		case "-":
			state.accepting = false
		case "+":
			state.accepting = true
		default:
			panic(fmt.Errorf(
				"invalid accepting flag in DFA file: \"%s\"",
				parts[0]))
		}
		stateID, err := strconv.Atoi(parts[1])
		if err != nil {
			panic(fmt.Errorf(
				"invalid state ID in DFA file: \"%s\"",
				parts[1]))
		}
		state.trans = make(map[byte]StateID)
		for i, p := range parts[2:] {
			if p == "E" {
				continue
			}

			transID, err := strconv.Atoi(p)
			if err != nil {
				panic(fmt.Errorf(
					"invalid state transition ID in DFA file: \"%s\"",
					p))
			}
			state.trans[alphabet[i]] = StateID(transID)
		}
		dfa.states[StateID(stateID)] = &state
	}

	return &dfa
}

func readScanDefnFile(f *os.File) ([]TokenDFA, Set) {
	tokenDFAs := make([]TokenDFA, 0)
	alphabet := make([]byte, 0)

	lineScanner := bufio.NewScanner(f)
	lineScanner.Scan()
	splitFunc := func(c rune) bool {
		return c == ' ' || c == '\t'
	}

	// read first line
	parts := strings.FieldsFunc(lineScanner.Text(), splitFunc)
	if len(parts) == 0 {
		panic(fmt.Errorf(
			"the first line of the scanner file cannot be empty"))
	}
	for _, p := range parts {
		for i := 0; i < len(p); i++ {
			if p[i] == 'x' {
				// Alphabet Encoding
				c, err := strconv.ParseInt(p[i+1:i+3], 16, 8)
				if err != nil {
					panic(err)
				}
				alphabet = append(alphabet, byte(c))
				i += 2
			} else {
				alphabet = append(alphabet, p[i])
			}
		}
	}

	// read remaining lines
	for lineScanner.Scan() {
		parts = strings.FieldsFunc(lineScanner.Text(), splitFunc)
		l := len(parts)
		if l == 0 {
			continue
		}
		if l < 2 || l > 3 {
			panic(fmt.Errorf(
				"there must be 2 or 3 fields per line in scan definition file; one has %d",
				l))
		}
		dfaFile, err := os.Open(parts[0])
		if err != nil {
			panic(err)
		}
		defer dfaFile.Close()
		tokenDFA := TokenDFA{
			tokenID: parts[1],
			dfa:     readDFATableFile(dfaFile, alphabet),
		}
		if l == 3 {
			tokenDFA.val = parts[2]
		}
		tokenDFAs = append(tokenDFAs, tokenDFA)
	}

	alphabetSet := make(Set)
	for _, c := range alphabet {
		alphabetSet.Add(c)
	}

	return tokenDFAs, alphabetSet
}
