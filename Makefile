GOSRC != find . -name '*.go'
GOSRC += go.mod
GO = go
RM ?= rm -f

LUTHER: $(GOSRC)
	$(GO) build -o $@

clean:
	$(RM) LUTHER

davidflorness.tar.gz: Makefile $(GOSRC)
	tar czvf $@ $^
