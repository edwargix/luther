package main

import (
	"fmt"
	"io"
	"os"
)

// hack to get the seek position of a file
func GetPos(f *os.File) int64 {
	offset, err := f.Seek(0, os.SEEK_CUR)
	if err != nil {
		panic(err)
	}
	return offset
}

type Token struct {
	id     string
	pos    int64
	line   int64
	column int64
	len    int64
	numNLs int64
	lastNL int64
	val    string
}

func findToken(f *os.File, tokenDFAs []TokenDFA, alphabet Set, last Token) Token {
	buf := make([]byte, 1)
	base := GetPos(f)       // we're only concerned with content past this mark
	longestToken := Token{} // has len of 0
	token := Token{
		pos:    1,
		line:   1,
		column: 1,
	}

	for _, tokenDFA := range tokenDFAs {
		f.Seek(base, os.SEEK_SET)

		token = Token{
			id:   tokenDFA.tokenID,
			pos:  last.pos + last.len,
			line: last.line + last.numNLs,
			val:  tokenDFA.val,
		}
		if last.numNLs > 0 {
			token.column = last.pos + last.len - last.lastNL
		} else {
			token.column = last.column + last.len
		}

		state := StateID(0)
		for {
			_, err := f.Read(buf)
			if err == io.EOF {
				break
			} else if err != nil {
				panic(err)
			} else if !alphabet.Contains(buf[0]) {
				panic(fmt.Errorf(
					"character not specified in alphabet: \"%c\"",
					buf[0]))
			}
			if buf[0] == '\n' {
				token.numNLs += 1
				token.lastNL = GetPos(f) - 1
			}
			if transID, ok := tokenDFA.dfa.states[state].trans[buf[0]]; ok {
				state = transID
				if tokenDFA.dfa.states[state].accepting {
					token.len = GetPos(f) - base
					if token.len > longestToken.len {
						longestToken = token
					}
				}
			} else {
				break
			}
		}
	}

	// get token's string value
	if longestToken.val == "" {
		f.Seek(base, os.SEEK_SET)
		longestToken.val = ""
		buf = make([]byte, longestToken.len)
		read := int64(0)
		for read < longestToken.len {
			n, err := f.Read(buf)
			if err != nil {
				panic(err)
			}
			read += int64(n)
			longestToken.val += alphabetEncode(buf)
		}
	} else {
		f.Seek(base+longestToken.len, os.SEEK_SET)
	}

	return longestToken
}

func tokenizeSourceFile(f *os.File, tokenDFAs []TokenDFA, alphabet Set) []Token {
	tokens := make([]Token, 0)
	buf := make([]byte, 1)
	last := Token{
		line:   1,
		column: 1,
	}

	for {
		_, err := f.Read(buf)
		if err == io.EOF {
			break
		} else if err != nil {
			panic(err)
		}
		f.Seek(-1, os.SEEK_CUR)
		last = findToken(f, tokenDFAs, alphabet, last)
		tokens = append(tokens, last)
	}

	return tokens
}
